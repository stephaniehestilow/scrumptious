from django.contrib import admin
from recipes.models import Recipe, RecipeStep, RecipeIngredient

# Register your models here.

# RECIPE MODEL REGISTRATION
@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "id",
    )


# RECIPE STEP MODEL REGISTRATION
@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = (
        "step_number",
        "instruction",
        "id",
    )


# RECIPE INGREDIENTS MODEL REGISTRATION
@admin.register(RecipeIngredient)
class RecipeIngredientAdmin(admin.ModelAdmin):
    list_display = (
        "amount",
        "food_item",
        "id",
    )
