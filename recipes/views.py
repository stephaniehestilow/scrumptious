from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm
from django.contrib.auth.decorators import login_required


# SHOW ALL OBJECTS IN MODEL
def recipe_list(request):
    recipes = Recipe.objects.all()  # Primary query for showing ALL objects
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)
    # Context will always connect with the html template


# SHOW OBJECTS WITH FILTER
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)


# SHOW SPECIFIC OBJECT IN MODEL
@login_required
def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)   # Primary query for showing ONE object
    context = {
        "recipe_object": recipe,
    }
    return render(request, "recipes/detail.html", context)


# FORM - CREATE NEW (Remember to import redirect, login required)
@login_required
def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)  # Save but NOT to the database
            recipe.author = request.user
            recipe.save()
            return redirect("recipe_list")
    else:
        form = RecipeForm()
    context = {
        "form": form,
    }
    return render(request, "recipes/create.html", context)


# FORM - EDIT EXISTING
def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=id)
    else:
        form = RecipeForm(instance=recipe)
    context = {
        "form": form,
        "recipe_object": recipe,
    }
    return render(request, "recipes/edit.html", context)



# DELETE OBJECT
